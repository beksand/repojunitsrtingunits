package pl.junit;

public class StringUtils {
    public static void main(String[] args) {
//        System.out.println(zamienNaDuzeZnaki("very good"));
//        System.out.println(odwrocString("dog"));
//        System.out.println(zwielokrotnijString("play", 3));
//        System.out.println(usunWielkieLitery("Adam Nowak"));
//        System.out.println(usunMaleLitery("Adam Nowak"));
    }
    public static String zamienNaDuzeZnaki(String str){
        return str.toUpperCase();
    }
    public static String odwrocString(String str){
        String tempStr="";
        for (int i = 0; i <str.length(); i++) {
            tempStr+=str.charAt(str.length()-1-i);
        }
        return tempStr;
    }
    public static String zwielokrotnijString(String str, int i){
        String tempStr="";
        for (int j = 0; j <i ; j++) {
            tempStr+=str+" ";
        }
        return tempStr;
    }
    public static String usunWielkieLitery(String str){
        String tempStr="";
        for (int i = 0; i <str.length() ; i++) {
            int ascii = (int) str.charAt(i);
            if ((ascii>96) && (ascii<123)){
                tempStr+=str.charAt(i);
            }
        }
        return tempStr;
    }
    public static String usunMaleLitery(String str){
        StringBuilder tempStr= new StringBuilder();
        for (int i = 0; i <str.length() ; i++) {
            int ascii = (int) str.charAt(i);
            if ((ascii>64) && (ascii<91)){
                tempStr.append(str.charAt(i));
            }
        }
        return tempStr.toString();
    }
    public static String usunZnakiAlfaNumeryczne (String str){
        return str;
    }

    public static int inedxLitery(String word, char lit){
        int index =-1;

        for (int i = 0; i < word.length(); i++) {
            if (lit==word.charAt(i)){
                index=i;
                break;
            }
        }
        return index;
    }
    public static int indexLitery2(String word, char lit){
        int index = -1;
        for (int i = word.length()-1; i >=0 ; i--) {
            if (lit==word.charAt(i)){
                index=i;
                break;
            }
        }
        return index;
    }
}
