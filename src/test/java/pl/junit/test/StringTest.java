package pl.junit.test;

import org.junit.Test;
import pl.junit.StringUtils;


import static org.junit.Assert.assertEquals;

public class StringTest {
    @Test
    public void dogNaDOG(){
       assertEquals("DOG", StringUtils.zamienNaDuzeZnaki("dog"));
    }
    @Test
    public void catNaCAT(){
        assertEquals("CAT", StringUtils.zamienNaDuzeZnaki("cat"));
    }
    @Test
    public void broNaOrb(){
        assertEquals("orb", StringUtils.odwrocString("bro"));
    }
    @Test
    public void yoNaOy(){
        assertEquals("oy", StringUtils.odwrocString("yo"));
    }
    @Test
    public void keyNakeykeykey(){
        assertEquals("key key key ", StringUtils.zwielokrotnijString("key",3));
    }
    @Test
    public void smartNasmartsmart(){
        assertEquals("smart smart ", StringUtils.zwielokrotnijString("smart",2));
    }
    @Test
    public void AdamNowakNaAN(){
        assertEquals("AN", StringUtils.usunMaleLitery("Adam Nowak"));
    }
    @Test
    public void JoshNaJ(){
        assertEquals("J", StringUtils.usunMaleLitery("Josh"));
    }
    @Test
    public void abcNaPusto(){
        assertEquals("",StringUtils.usunMaleLitery("abc"));
    }
    @Test
    public void indexLiteryAwSlowieAla(){
        assertEquals(0,StringUtils.inedxLitery("ala", 'a'));
    }
    @Test
    public void indexLiteryRwSlowieBro(){
        assertEquals(1,StringUtils.inedxLitery("bro", 'r'));
    }
    @Test
    public void ostIndexLiteryAwSlowieAla(){
        assertEquals(2, StringUtils.indexLitery2("ala", 'a'));
    }
}
